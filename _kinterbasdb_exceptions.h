/* KInterbasDB Python Package - Header File with References to DB-API Exceptions
 *
 * Version 3.3
 *
 * The following contributors hold Copyright (C) over their respective
 * portions of code (see license.txt for details):
 *
 * [Original Author (maintained through version 2.0-0.3.1):]
 *   1998-2001 [alex]  Alexander Kuznetsov   <alexan@users.sourceforge.net>
 * [Maintainers (after version 2.0-0.3.1):]
 *   2001-2002 [maz]   Marek Isalski         <kinterbasdb@maz.nu>
 *   2002-2007 [dsr]   David Rushby          <woodsplitter@rocketmail.com>
 * [Contributors:]
 *   2001      [eac]   Evgeny A. Cherkashin  <eugeneai@icc.ru>
 *   2001-2002 [janez] Janez Jere            <janez.jere@void.si>
 */

/* This module provides access to the standard DB API exceptions types defined
 * by kinterbasdb (the types are accessible through global variables).
 * A typical kinterbasdb exception might be raised like this:
 *   raise_exception(ProgrammingError, "This is the error message.");
 *
 * WARNING:
 *   The global pointers to DB API exception types *declared* here (not
 * *defined* here) are only meant to be implicitly referenced in code that
 * compiles into the main kinterbasdb shared library (_kinterbasdb.dll or
 * _kinterbasdb.so).
 *   Code such as _kiservices.c that compiles into a separate shared library
 * cannot refer to the exception types simply by including this header file.
 * Instead, such code must import the main _kinterbasdb shared library (where
 * the exception types reside) and refer to the exception types via
 * _kinterbasdb.  For an example, see services.py and _kiservices.c.         */

#ifndef _KINTERBASDB_EXCEPTIONS_H
#define _KINTERBASDB_EXCEPTIONS_H

/* YYY: 2006.01.21: MinGW used to have a problem with FB 2.0's fb_interpret
 * function, but no longer (with MinGW-GCC-3.4.5 and FB 2.0.0.12174).
#if (defined(FIREBIRD_2_0_OR_LATER) && !defined(COMPILER_IS_MINGW_WIN32))
 */
#ifdef FIREBIRD_2_0_OR_LATER
  #define USE_MODERN_INTERP_FUNC
#endif

#ifndef isc_stack_trace
  #define isc_stack_trace 335544842L
#endif


#include "Python.h"

extern PyObject *Warning;
extern PyObject *Error;
extern PyObject *InterfaceError;
extern PyObject *DatabaseError;
extern PyObject *DataError;
extern PyObject *OperationalError;
extern PyObject *TransactionConflict;
extern PyObject *ConduitWasClosed;
extern PyObject *ConnectionTimedOut;
extern PyObject *IntegrityError;
extern PyObject *InternalError;
extern PyObject *ProgrammingError;
extern PyObject *NotSupportedError;

#endif /* _KINTERBASDB_EXCEPTIONS_H */
