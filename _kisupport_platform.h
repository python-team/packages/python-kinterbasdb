/* KInterbasDB Python Package - Header File for Platform-Specific Support
 *
 * Version 3.3
 *
 * The following contributors hold Copyright (C) over their respective
 * portions of code (see license.txt for details):
 *
 * [Original Author (maintained through version 2.0-0.3.1):]
 *   1998-2001 [alex]  Alexander Kuznetsov   <alexan@users.sourceforge.net>
 * [Maintainers (after version 2.0-0.3.1):]
 *   2001-2002 [maz]   Marek Isalski         <kinterbasdb@maz.nu>
 *   2002-2007 [dsr]   David Rushby          <woodsplitter@rocketmail.com>
 * [Contributors:]
 *   2001      [eac]   Evgeny A. Cherkashin  <eugeneai@icc.ru>
 *   2001-2002 [janez] Janez Jere            <janez.jere@void.si>            */

#ifndef _KISUPPORT_PLATFORM_H
#define _KISUPPORT_PLATFORM_H

#include "_kinterbasdb.h"
#include "pythread.h"

#ifdef PLATFORM_WINDOWS

  #include <windows.h>

  typedef DWORD PlatformThreadIdType;

  typedef HANDLE PlatformThreadRefType;
  #define THREAD_REF_INVALID NULL
  typedef LPTHREAD_START_ROUTINE PlatformThreadFuncType;
  #define THREAD_FUNC_MODIFIER WINAPI

  typedef DWORD PlatformThreadFuncReturnType;
  #define THREAD_FUNC_RETURN_SUCCESS  0
  #define THREAD_FUNC_RETURN_FAILURE  1

  typedef CRITICAL_SECTION PlatformMutexType;

#else /* If not Windows, assume POSIX. */

  #include <sys/types.h>
  #include <unistd.h>
  #include <pthread.h>

  typedef pthread_t PlatformThreadIdType;

  typedef pthread_t PlatformThreadRefType;
  #define THREAD_REF_INVALID -1
  typedef void * (*PlatformThreadFuncType)(void *);
  #define THREAD_FUNC_MODIFIER

  typedef void * PlatformThreadFuncReturnType;
  /* THREAD_FUNC_RETURN_SUCCESS is a meaningless, but non-NULL, pointer
   * value: */
  #define THREAD_FUNC_RETURN_SUCCESS \
    ((PlatformThreadFuncReturnType) (&null_connection))
  #define THREAD_FUNC_RETURN_FAILURE \
    ((PlatformThreadFuncReturnType) NULL)

  typedef pthread_mutex_t PlatformMutexType;

  static void millis_into_future_to_abstime(
      long millis, struct timespec *abstime
    );

#endif

#define THREAD_ID_NONE ((PlatformThreadIdType) 0)

#endif /* if not def _KISUPPORT_PLATFORM_H */
