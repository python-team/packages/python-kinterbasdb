#include "_kisupport.h"

#ifdef PLATFORM_WINDOWS
  #include "_kisupport_platform_windows.c"
#else
  #include "_kisupport_platform_posix.c"
#endif
